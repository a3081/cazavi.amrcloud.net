(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["url"] = "http://localhost:5000/api/";
  window["env"]["alias"] = "cazavi";
  window["env"]["production"] = false;
  window["env"]["googleAnalytics"] = '';

})(this);
