(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["url"] = "${URL}";
  window["env"]["alias"] = "${ALIAS}";
  window["env"]["production"] = "${PRODUCTION}";
  window["env"]["googleAnalytics"] = "${GOOGLE_ANALYTICS}";

})(this);
